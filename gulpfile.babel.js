import gulp from 'gulp';
import browserSync from 'browser-sync';
import del from 'del';
import runSequence from 'run-sequence';
import wiredeps from 'wiredep'; // AUTO INJECT BOWER COMPONENTS TO THE HTML
import gulpLoadPlugins from 'gulp-load-plugins'; // AUTO INJECT BOWER COMPONENTS TO THE HTML

const $ = gulpLoadPlugins(),
    wiredep = wiredeps.stream, // AUTO INJECT BOWER COMPONENTS TO THE HTML
    cheerio = require('gulp-cheerio'),
    path = {dev: 'app/', tmp: '.tmp/', build: 'dist/'},
    env = {dev: $.environments.development, prod: $.environments.production},
    reload = browserSync.reload,
    nunjucksPartialsFolder = 'partials';
let partialFileChanged = false;

gulp.task('clean:build', () => del([path.build + '*']));
gulp.task('clean:tmp', () => del([path.tmp + '*']));
gulp.task('clean:js', () => del([path.tmp + 'scripts/']));
gulp.task('copy:fonts', () => {
  return gulp.src(
      ['bower_components/**/*.{eot,svg,ttf,woff,woff2}', 'assets/fonts/*'])
             .pipe($.flatten())
             .pipe(gulp.dest(path.build + 'fonts/'));
});
gulp.task('copy:imgs', () => {
  
  let plugins = [
    $.imagemin.gifsicle(),
    $.imagemin.jpegtran(),
    $.imagemin.optipng()];
  
  return gulp.src(['assets/images/**/*'])
             .pipe($.plumber())
             .pipe($.imagemin(plugins, {progressive: true, interlaced: true}))
             .pipe($.size({title: 'images'}))
             .pipe(gulp.dest(path.build + 'assets/images/'));
});
gulp.task('copy:js', () => {
  return gulp.src(path.dev + 'scripts/*.js')
             .pipe($.plumber())
             .pipe($.babel({
                             presets: ['es2015'],
                           }))
             .on('error', function(e) {
               console.error(e);
               this.emit('end');
             })
             .pipe($.changed(path.tmp + 'scripts/'))
             .pipe(gulp.dest(path.tmp + 'scripts/'));
});
gulp.task('watch', () => {
  gulp.watch(path.dev + '**/*.nunjucks')
      .on('change', function(e) {
        if (e.path.includes(nunjucksPartialsFolder))
          partialFileChanged = true; //Compile all nunjucks files
        else
          partialFileChanged = false; //Compile only changed nunjucks file
    
        runSequence('nunjucks');
      });
  gulp.watch(path.dev + 'styles/**/*.scss', ['sass']);
  gulp.watch(path.dev + 'scripts/*.js', ['copy:js']);
  gulp.watch(path.dev + 'images/**/*', ['copy:imgs']);
  gulp.watch([
               path.tmp + '*.html',
               path.tmp + 'scripts/*.js',
             ])
      .on('change', reload);
});
gulp.task('browserSync', () => {
  browserSync.init({
                     server: {
                       baseDir: path.tmp,
                       routes: {
                         '/bower_components': 'bower_components',
                         '/assets': 'assets',
                       },
                     },
                   });
});
gulp.task('svgstore', () => {
  return gulp.src('assets/images/svgs/icons/*')
             .pipe($.plumber())
             .pipe($.svgmin(function(file) {
               return {
                 plugins: [
                   {
                     cleanupIDs: {
                       minify: true,
                     },
                   }],
               };
             }))
             .pipe($.svgstore({inlineSvg: true}))
             .pipe($.rename('icons-set.svg'))
             .pipe(cheerio({
                             run: function($) {
                               $('svg')
                                   .css('display', 'none');
                               $('svg')
                                   .find('defs')
                                   .remove();
                               $('[fill]')
                                   .removeAttr('fill');
                             },
                             parserOptions: {xmlMode: true},
                           }))
             .pipe($.size({title: 'icons-set'}))
             .pipe(gulp.dest('assets/images/svgs/'));
});
gulp.task('sass', () => {
  return gulp.src(path.dev + 'styles/*.scss')
             .pipe($.plumber())
             .pipe(wiredep()) //TO IMPORT BOOTSTRAP SASS FROM BOWER FOLDER INTO LAYOUT.SCSS
             .pipe($.sourcemaps.init())
             .pipe($.sass()
                    .on('error', $.sass.logError))
             .pipe($.autoprefixer({
                                    browsers: ['> 0%'],
                                    remove: false,
                                  }))
             .pipe($.sourcemaps.write('.'))
             .pipe(gulp.dest(path.tmp + 'styles/'))
             .pipe(browserSync.stream({match: '**/*.css'}));
});
gulp.task('nunjucks', () => { //RENDER HTML
  var dataObject = {
    path: ['./app/'], data: {}, envOptions: {
      tags: {
        // blockStart: '{%',
        // blockEnd: '%}',
        // variableStart: '{$',
        // variableEnd: '$}'
        // commentStart: '{#',
        // commentEnd: '#}'
      },
    },
  };
  if (env.dev()) dataObject.data = {assets: '../assets/'};
  
  function fileContents(filePath, file) {
    return file.contents.toString();
  }
  
  return gulp.src(path.dev + '*.nunjucks')
             .pipe($.plumber())
             .pipe($.if(!partialFileChanged, env.dev($.changed(path.tmp, {
               extension: '.html',
             }))))
             .pipe($.nunjucksRender(dataObject))
             .pipe(wiredep()) //IMPORT BOWER PACKAGES INTO HTML
             .pipe($.inject(gulp.src('./assets/images/svgs/icons-set.svg'),
                            {transform: fileContents}))
             .pipe(env.prod($.useref())) //CONCATENATE JS / CSS FILES AND MINIFY THEM
             .pipe(env.prod($.if('*.js', $.uglify())))
             .pipe(env.prod($.if('*.css', $.cleanCss())))
             .pipe(env.prod(
                 $.if('*.html', $.htmlmin({collapseWhitespace: true}))))
             .pipe(env.dev(gulp.dest(path.tmp)))
             .pipe(env.prod(gulp.dest(path.build)));
});
gulp.task('serve', () => {
  $.environments.current(env.dev);
  runSequence('clean:tmp', 'sass', 'copy:js', 'nunjucks', 'watch',
              'browserSync');
});
gulp.task('build', () => {
  $.environments.current(env.prod);
  runSequence('clean:build', 'sass', 'copy:js', 'nunjucks',
              ['copy:imgs', 'copy:fonts']);
});